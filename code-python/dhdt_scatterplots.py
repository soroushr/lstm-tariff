import os, sys
import calendar
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import statsmodels.api as sm
from skmisc.loess import loess
plt.rc('font', family='serif')

lowess = sm.nonparametric.lowess
## define paths
WELL_DATA_PATH_MODIFIED = os.getcwd() + '../data/well-time-series-modified/'

df = pd.DataFrame()
for csv_well_data in os.listdir(WELL_DATA_PATH_MODIFIED):
    if os.path.splitext(csv_well_data)[-1].lower() != '.csv':
        continue

    csv_name = os.path.basename(csv_well_data)

    well_data = pd.read_csv(WELL_DATA_PATH_MODIFIED + csv_well_data)

    all_vars = well_data[['tmax (deg c)', 'dhdt', 'prcp (mm/day)']].dropna()

    print(df.shape)
    df = df.append(all_vars, ignore_index=True)
    print(df.shape)
    print('\n')


## plot temperature vs dhdt
z = lowess(df['dhdt'], df['tmax (deg c)'], frac=1/3., it=0, return_sorted=True)
plt.plot(z[:,0], z[:,1], 'b--',lw=3,label='smoothed')
plt.xlabel(r"$T_{\mathrm{max}}$ [$^{\circ}$C]")
plt.ylabel(r"$\frac{dL}{dt}$ [m/day]")
plt.grid(linestyle='dotted')
plt.legend()
plt.savefig('../figs/dhdt-vs-temp-empty.png', dpi=250)
plt.clf()

df['dhdht-temp-loess'] = z[:,1]
df['tmax-temp-loess'] = z[:,0]

z = lowess(df['dhdt'], df['prcp (mm/day)'], frac=1/3., it=0, return_sorted=True)
plt.plot(z[:,0], z[:,1], 'b--',lw=3,label='smoothed')
plt.xlabel("precipitation [mm/day]")
plt.ylabel(r"$\frac{dL}{dt}$ [m/day]")
plt.grid(linestyle='dotted')
plt.legend()
plt.savefig('../figs/dhdt-vs-precip-empty.png', dpi=250)
plt.clf()

df['dhdt-prcp-loess']=z[:,1]
df['prcp-prcp-loess']=z[:,0]
print(df.head())
df.to_csv('for_Mani.csv')
