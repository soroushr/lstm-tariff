import numpy as np
import pandas as pd
import os
import calendar
import sys
import matplotlib.pyplot as plt
import tensorflow as tf
import keras
from keras import layers
from util import stack_all_wells
from scipy.stats import linregress

plt.rc('font', family='serif')
limit = 5

WELL_DATA_PATH_MODIFIED = os.getcwd() + '../data/well-time-series-modified/'

all_wells = stack_all_wells(path = WELL_DATA_PATH_MODIFIED,
                            resampling = None,
                            dropna = False)

# after March, before October
all_wells = all_wells[(all_wells['yday']>90) & (all_wells['yday']<260)]

# interpolate corn prices
all_wells['price_corn'] = all_wells['price_corn'].interpolate(method='from_derivatives',
                                                              limit_direction='both', limit=5)

all_wells['deltaT'] = all_wells['tmax (deg c)'] - all_wells['tmin (deg c)']

# add shifts from 1 day to 1 week as new
# features for all climate variables

shifts = range(1,8)
for shift in shifts:
    prcp = 'dprcpdt' + str(shift)
    tmax = 'dtmaxdt' + str(shift)
    tmin = 'dtmindt' + str(shift)
    delT = 'ddeltaTdt' + str(shift)
    dswe = 'swe (kg/m^2)' + str(shift)
    dvp  = 'vp (Pa)' + str(shift)    

    all_wells[prcp] = all_wells['prcp (mm/day)'].diff(shift)
    all_wells[tmax] = all_wells['tmax (deg c)'].diff(shift)
    all_wells[tmin] = all_wells['tmin (deg c)'].diff(shift)
    all_wells[delT] = all_wells['deltaT'].diff(shift)
    all_wells[dswe] = all_wells['swe (kg/m^2)'].diff(shift)
    all_wells[dvp]  = all_wells['vp (Pa)'].diff(shift)

all_wells = all_wells.dropna()
all_wells_subset = all_wells[(all_wells['dhdt']<limit) & (all_wells['dhdt']>-limit)]

all_wells_subset = all_wells_subset.reset_index()

## train test splitting
train_dataset = all_wells_subset.sample(frac=0.70, random_state=None)
test_dataset = all_wells_subset.drop(train_dataset.index)

train_stats = train_dataset.describe()
train_stats.pop("dhdt")
train_stats = train_stats.transpose()

train_labels = train_dataset.pop('dhdt')
test_labels = test_dataset.pop('dhdt')

#normalize
def norm(x):
  return (x - train_stats['mean'])/train_stats['std']

normed_train_data = norm(train_dataset)
normed_test_data = norm(test_dataset)

# model dev
def build_model():
    model = keras.Sequential([
    layers.Dense(512, kernel_initializer='random_uniform', activation=tf.nn.relu, input_shape=[len(train_dataset.keys())]),
    layers.Dropout(0.10),
    layers.Dense(512),
    layers.Dropout(0.10),
    layers.Dense(256),
    layers.Dropout(0.10),
    layers.Dense(256),
    layers.Dropout(0.10),
    layers.Dense(256),
    layers.Dropout(0.10),
    layers.Dense(256),
    layers.Dropout(0.10),
    layers.Dense(256),
    layers.Dropout(0.10),
    layers.Dense(256),
    layers.Dropout(0.10),
    layers.Dense(256),
    layers.Dropout(0.10),
    layers.Dense(256),
    layers.Dropout(0.10),
    layers.Dense(256),
    layers.Dropout(0.10),
    layers.Dense(256),
    layers.Dropout(0.10),
    layers.Dense(128),
    layers.Dropout(0.75),
    layers.Dense(16),
    layers.Dropout(0.01),
    layers.Dense(1)
    ])

#    optimizer = keras.optimizers.RMSprop(0.0005)
#    optimizer = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0, amsgrad=False)
    optimizer = keras.optimizers.Adam(lr=0.000005, beta_1=0, beta_2=0, epsilon=None, decay=0, amsgrad=False)
#    optimizer = keras.optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)

    model.compile(loss='mean_squared_error',
                optimizer=optimizer,
                metrics=['mean_squared_error'])
    return model

model = build_model()
early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

# go train
# Display training progress by printing a single dot for each completed epoch
class PrintDot(keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs):
        if epoch % 10 == 0: print('')
        print('.', end='')

EPOCHS = 20

history = model.fit(normed_train_data, train_labels, epochs=EPOCHS,
                    validation_split = 0.2, verbose=1, callbacks=[PrintDot()],
#                    validation_split = 0.2, verbose=1, callbacks=[early_stop, PrintDot()],
                    batch_size=32)

hist = pd.DataFrame(history.history)
hist['epoch'] = history.epoch
#hist.tail()

def plot_history(history):
    hist = pd.DataFrame(history.history)
    hist['epoch'] = history.epoch

    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('MSE [m/day$^2$]')
    plt.plot(hist['epoch'], hist['mean_squared_error'],
           label='Train Error')
    plt.plot(hist['epoch'], hist['val_mean_squared_error'],
           label = 'Val Error')
    plt.legend()
    plt.grid(linestyle='dotted')
    plt.savefig('../figs/keras-loss.png',dpi=300)
    plt.clf()

plot_history(history)

test_predictions = model.predict(normed_test_data).flatten()

lin = linregress(test_labels, test_predictions)

scatter_args = dict(color='r', s=1,
                    label='r$^2$={:0.2f}'.format(lin.rvalue) )
plt.scatter(test_labels, test_predictions, **scatter_args)
plt.xlabel('True dhdt')
plt.ylabel('Predicted dhdt')
plt.axis('equal')
plt.axis('square')
plt.grid(linestyle='dotted')
plt.xlim([-limit,limit])
plt.ylim([-limit,limit])
plt.legend()
plt.savefig('../figs/keras-prediction.png', dpi=300)
