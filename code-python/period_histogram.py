import os, sys
import calendar
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.rc('font', family='serif')

## define paths
WELL_DATA_PATH_MODIFIED = os.getcwd() + '../data/well-time-series-modified/'

#####################
all_dates = []
for csv_well_data in os.listdir(WELL_DATA_PATH_MODIFIED):
    if os.path.splitext(csv_well_data)[-1].lower() != '.csv':
        continue

    csv_name = os.path.basename(csv_well_data)

    well_data = pd.read_csv(WELL_DATA_PATH_MODIFIED + csv_well_data)

    first_idx = well_data.X_72019_00001.first_valid_index()
    last_idx = well_data.X_72019_00001.last_valid_index()
    site_no = well_data.site_no[0]

    if not pd.isna(first_idx):
        first_date = well_data.Date[first_idx]
        last_date = well_data.Date[last_idx]

        if int(last_date.split('-')[0])>2017:
            first_year = int(first_date.split('-')[0])
            last_year = int(last_date.split('-')[0])

            print("\nsite_no:{}, first year:{}, last year:{}".format(site_no, first_year, last_year) )

            all_dates.append([x for x in range(first_year, last_year+1)])

flat_list = np.asarray([item for sublist in all_dates for item in sublist])

unique, counts =  np.unique(flat_list, return_counts=True)
_dict = dict(zip(unique, counts))

hist_args = {'facecolor':'g', 'edgecolor':'k', 'linewidth':.2}
plt.bar(_dict.keys(), _dict.values(), width=1, **hist_args)
plt.xticks(np.arange(min(flat_list), max(flat_list)+1, 1.0) )
plt.xticks(rotation=90)
plt.savefig("./figs/hist-w-2018.png", dpi=300)
plt.clf()
