import numpy as np
import pandas as pd
import os
import calendar
import sys
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import tensorflow as tf
import xgboost as xgb
#from util import stack_all_wells
from sklearn.model_selection import train_test_split
from scipy.stats import linregress
import crops_dict


plt.rc('font', family='serif')
limit = 4

WELL_DATA_PATH_MODIFIED = os.getcwd() + '../data/well-time-series-HPA-modified/'
path = WELL_DATA_PATH_MODIFIED

crops = crops_dict.crops

unwanted_params = [#'Date_Month',
                   'year',
                   #'X_72019_00001',
                   'SiteNo',
                   'US_State',
                   'dwaterlevel_dt']

for csv_well_data in os.listdir(WELL_DATA_PATH_MODIFIED)[::-1]:
    if os.path.splitext(csv_well_data)[-1].lower() != '.csv':
        continue

    csv_name = os.path.basename(csv_well_data)
    site_number = csv_name.split("_")[0]
    print("csv name is: {}".format(csv_name) )
    print("SiteNo is: {}".format(site_number) )

    well_data = pd.read_csv(WELL_DATA_PATH_MODIFIED + csv_well_data)#, parse_dates=['date'])
    well_data = well_data.drop(unwanted_params, axis=1)

#    well_data['price_corn'] = well_data['price_corn'].interpolate(method='from_derivatives',
#                                                              limit_direction='both', limit=5)

    well_data = well_data.dropna()

    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    print("length is: {}".format(len(well_data)) )
    print("csv name is: {}".format(csv_name) )
    print("##################################")
    if len(well_data)<500:
        continue

    FIVE_PERCENTILE = 3500 # NO. of pixels
    unwanted_crops = []
    for crop_id in crops:
        if np.max(well_data[crops[crop_id]]) < FIVE_PERCENTILE:
            unwanted_crops.append(crops[crop_id])
    well_data = well_data.drop(unwanted_crops, axis=1)
    print("the dropped crops are: {}".format(unwanted_crops) )

    well_data.loc[:, ~well_data.columns.str.contains('^Unnamed')]

    # after March, before October
    well_data = well_data[(well_data['yday']>90) & (well_data['yday']<260)]
    well_data['deltaT'] = well_data['tmax (deg c)'] - well_data['tmin (deg c)']

    # add shifts from 1 day to 1 week as new
    # features for all climate variables
    shifts = range(1,8)
    for shift in shifts:
        prcp = 'dprcpdt' + str(shift)
        tmax = 'dtmaxdt' + str(shift)
        tmin = 'dtmindt' + str(shift)
        delT = 'ddeltaTdt' + str(shift)

        well_data[prcp] = well_data['prcp (mm/day)'].diff(shift)
        well_data[tmax] = well_data['tmax (deg c)'].diff(shift)
        well_data[tmin] = well_data['tmin (deg c)'].diff(shift)
        well_data[delT] = well_data['deltaT'].diff(shift)

    well_data = well_data.dropna()
    well_data_subset = well_data[(well_data['dDTW_dt']<limit) & (well_data['dDTW_dt']>-limit)]

    well_data_subset = well_data_subset.reset_index()

    ## train test splitting
    drops = ['dDTW_dt', 'date']#, 'srad (W/m^2)']
    X, y = well_data_subset.drop(drops, axis=1), well_data_subset['dDTW_dt']

    if len(X)<2:
        continue

#    data_dmatrix = xgb.DMatrix(data=X,label=y)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=123)

    ## XBOOST
    def init_xgboost(n_estimator=5000, max_depth=10, lr=0.009, nthread=4):
        xg_reg = xgb.XGBRegressor(objective='reg:squarederror', colsample_bytree=0.99,
                                  learning_rate=lr, max_depth=max_depth,
                                  alpha=2, n_estimators=n_estimator, verbosity=0,
                                  nthread=nthread, eta=0.1)

        return xg_reg

    def train_xgboost(X_train, y_train, reg):
        reg.fit(X_train, y_train)

        return reg    

    def save_prediction(reg, s=20):
    #    reg.fit(X_train, y_train)
        mse = mean_squared_error(y_test, reg.predict(X_test))
        print("MSE: %.4f" % mse)
        rmse = np.sqrt(mse)

        lin = linregress(y_test, reg.predict(X_test))
        score = reg.score(X_test, y_test)

        lim = [-limit,limit]
        scatter_args = dict(color='r', s=s,
                            label='r$^2$={:0.2f}\nRMSE={:0.4f}'.format(lin.rvalue, rmse),
                            edgecolors='w', alpha=0.75, linewidth=0.5)
        plt.scatter(y_test, reg.predict(X_test), **scatter_args)
        plt.gca().set_aspect('equal')
        plt.grid(linestyle='dotted')
        plt.xlim(lim)
        plt.ylim(lim)
        plt.xlabel('True dDTW_dt (ft/day)')
        plt.ylabel('Predicted dDTW_dt (ft/day)')
        plt.plot(lim, lim, 'grey', alpha=.3, linewidth=1)
        plt.legend()
        plt.savefig('./figs/wells-ML-HPA/{}.png'.format(csv_name[:-4]), dpi=300)
        plt.clf()

    x_reg = init_xgboost(n_estimator=1000, lr=0.4, nthread=10)
    x_reg = train_xgboost(X_train, y_train, x_reg)
    save_prediction(x_reg)

