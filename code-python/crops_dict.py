crops = {
 "0" : "Background",
 "1" : "Corn",
 "2" : "Cotton",                                    
 "3" : "Rice",
 "4" : "Sorghum",
 "5" : "Soybeans",
 "6" : "Sunflower",                                    
 "10" : "Peanuts",
 "12" : "Sweet Corn",
 "13" : "Pop or Orn Corn",
 "21" : "Barley",
 "22" : "Durum Wheat",
 "23" : "Spring Wheat",
 "24" : "Winter Wheat",
 "25" : "Other Small Grains",
 "26" : "Dbl Crop WinWht/Soybeans",
 "27" : "Rye",
 "28" : "Oats",
 "29" : "Millet",
 "30" : "Speltz",
 "31" : "Canola",
 "33" : "Safflower",
 "36" : "Alfalfa",
 "37" : "Other Hay/Non Alfalfa",
 "41" : "Sugarbeets",
 "42" : "Dry Beans",
 "43" : "Potatoes",
 "44" : "Other Crops",
 "47" : "Misc Vegs & Fruits",
 "50" : "Cucumbers",
 "52" : "Lentils",
 "53" : "Peas",
 "57" : "Herbs",
 "59" : "Sod/Grass Seed",
 "60" : "Switchgrass",
 "61" : "Fallow/Idle Cropland",
 "63" : "Forest",
 "74" : "Pecans",
 "81" : "Clouds/No Data",
 "82" : "Developed",
 "83" : "Water",
 "87" : "Wetlands",
 "88" : "Nonag/Undefined",
 "111" : "Open Water",
 "121" : "Developed/Open Space",
 "122" : "Developed/Low Intensity",
 "123" : "Developed/Med Intensity",
 "124" : "Developed/High Intensity",
 "131" : "Barren",
 "141" : "Deciduous Forest",
 "142" : "Evergreen Forest",
 "143" : "Mixed Forest",
 "152" : "Shrubland",
 "176" : "Grass/Pasture",
 "190" : "Woody Wetlands",
 "195" : "Herbaceous Wetlands",
 "205" : "Triticale",
 "209" : "Cantaloupes",
 "225" : "Dbl Crop WinWht/Corn",
 "226" : "Dbl Crop Oats/Corn",
 "229" : "Pumpkins",
 "235" : "Dbl Crop Barley/Sorghum",
 "236" : "Dbl Crop WinWht/Sorghum",
 "237" : "Dbl Crop Barley/Corn",
 "238" : "Dbl Crop WinWht/Cotton",
 "241" : "Dbl Crop Corn/Soybeans",
}
