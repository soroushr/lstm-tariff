import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
import os, sys
from math import sqrt
from numpy import concatenate
from pandas import read_csv, DataFrame, concat
from sklearn.preprocessing import MinMaxScaler, LabelEncoder
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import Dense, LSTM

plt.rc('font', family='serif')

EPOCHS = 200

WELL_DATA_PATH_MODIFIED = os.getcwd() + '/../data/well-time-series-HPA-modified/'
CSV_NAME = "400155101521302_11740_lat_40.0286666666667_lon_-101.871222222222_2019-10-19_232352_modified.csv"

data = pd.read_csv(WELL_DATA_PATH_MODIFIED + CSV_NAME)

data.index = pd.to_datetime(data["date"])

"""
#plt.plot(range(data.shape[0]), data.wa_km)
plt.figure(figsize=(15,10))
fig_args = {'color':'k',
            'marker':'o',
            #'markeredgecolor':'w',
            'markersize':5,
            'linewidth':0.5,
            'alpha':0.8}
plt.plot(data.index, data.DTW_surface_ft, **fig_args)
plt.grid(linestyle='dotted')
plt.xlabel('date')
plt.ylabel('DTW [ft] for SiteNo: {}'.format(CSV_NAME.split("_")[0]))
plt.savefig("zz_test.png", dpi=300)
"""
dataset = data[['DTW_surface_ft',
                'prcp (mm/day)',
                'srad (W/m^2)',
                'swe (kg/m^2)',
                'tmax (deg c)',
                'tmin (deg c)',
                'vp (Pa)',
                'number_of_wells',
                'avg_pump_rate']]

dataset.index.name = 'date'
#dataset.index = pd.to_datetime(data["date"])
#dataset['DTW_surface_ft'].fillna(0, inplace=True)
dataset['DTW_surface_ft'] = dataset['DTW_surface_ft'].interpolate(method='from_derivatives',
                                                          limit_direction='both', limit=5)

# convert series to supervised learning
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    """
    Frame a time series as a supervised learning dataset.
    Arguments:
        data: Sequence of observations as a list or NumPy array.
        n_in: Number of lag observations as input (X).
        n_out: Number of observations as output (y).
        dropnan: Boolean whether or not to drop rows with NaN values.
    Returns:
        Pandas DataFrame of series framed for supervised learning.
    """
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


###################
###################
###################
values = dataset.values
values = values.astype('float32')
scaler = MinMaxScaler(feature_range=(0, 1))
scaled = scaler.fit_transform(values)
reframed = series_to_supervised(scaled, 1, 1)

#reframed.drop(reframed.columns[[9,10,11,12,13,14,15]], axis=1, inplace=True)
to_drop = list(range(reframed.shape[1]-1,values.shape[1],-1))
reframed.drop(reframed.columns[to_drop], axis=1, inplace=True)

#################
values = reframed.values
n_years = 5
n_train_days = 365 * n_years #* n_years
train = values[:n_train_days, :]
test = values[n_train_days:, :]
# split into input and outputs
train_X, train_y = train[:, :-1], train[:, -1]
test_X ,  test_y = test[:, :-1] , test[:, -1]
# reshape input to be 3D [samples, timesteps, features]
train_X = train_X.reshape((train_X.shape[0], 1, train_X.shape[1]))
test_X  = test_X.reshape((test_X.shape[0], 1, test_X.shape[1]))
print(train_X.shape, train_y.shape, test_X.shape, test_y.shape)

###########
# design network
model = Sequential()
model.add(LSTM(50, input_shape=(train_X.shape[1], train_X.shape[2])))
model.add(Dense(1))
model.compile(loss='mae', optimizer='adam')
# fit network
history = model.fit(train_X, train_y, epochs=EPOCHS, batch_size=72, validation_data=(test_X, test_y), verbose=1, shuffle=False)
# plot history
plt.plot(history.history['loss'], label='train')
plt.plot(history.history['val_loss'], label='test')
plt.legend()
plt.grid(linestyle='dotted')
plt.savefig('../zz_loss.png', dpi=300)
plt.clf()

##########
# make a prediction
yhat   = model.predict(test_X)
test_X = test_X.reshape((test_X.shape[0], test_X.shape[2]))
# invert scaling for forecast
inv_yhat = concatenate((yhat, test_X[:, 1:]), axis=1)
print("inv_yhat shape is: ", inv_yhat.shape)
#sys.exit()
inv_yhat = scaler.inverse_transform(inv_yhat)
inv_yhat = inv_yhat[:,0]
# invert scaling for actual
test_y = test_y.reshape((len(test_y), 1))
inv_y  = concatenate((test_y, test_X[:, 1:]), axis=1)
inv_y  = scaler.inverse_transform(inv_y)
inv_y  = inv_y[:,0]
# calculate RMSE
rmse = sqrt(mean_squared_error(inv_y, inv_yhat))
print('Test RMSE: %.3f' % rmse)

################################
## plot the test and forecast ##
## sets separately #############
plt.clf()
plt.plot(dataset.index[-inv_y.shape[0]:], inv_y,'red',label='test',alpha=0.8, linewidth=1)
plt.ylim([15, 32])
plt.legend(loc=1)
plt.grid(linestyle='dotted')
plt.savefig('../zz_test.png', dpi=300)

plt.clf()
plt.plot(dataset.index[-inv_y.shape[0]:], inv_yhat,'blue',label='forecast',alpha=0.8, linewidth=1)
plt.ylim([15, 32]) 
plt.legend(loc=1)
plt.grid(linestyle='dotted')
plt.savefig('../zz_forecast.png', dpi=300) 

################################
## plot error wrt. temperature##
################################
plt.plot(dataset["tmax (deg c)"][-inv_y.shape[0]:], inv_y-inv_yhat,'green', label='tmax',alpha=0.25, linestyle='None', marker='o', markersize=4)
plt.plot(dataset["tmin (deg c)"][-inv_y.shape[0]:], inv_y-inv_yhat,'purple',label='tmin',alpha=0.25, linestyle='None', marker='s', markersize=4)
plt.legend(loc=2)
plt.xlabel("temperature (deg C)")
plt.ylabel("DTW error")
plt.grid(linestyle='dotted')
plt.savefig('../zz_temp_vs_error.png', dpi=300) 
plt.clf()

