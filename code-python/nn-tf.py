import numpy as np
import pandas as pd
import os
import calendar
import sys
import tensorflow as tf 
import matplotlib.pyplot as plt
import seaborn as sns

from util import stack_all_wells

plt.rc('font', family='serif')

np.random.seed(101) 
tf.set_random_seed(101) 

WELL_DATA_PATH_MODIFIED = os.getcwd() + '../data/well-time-series-modified/'

drop_parameters = ['Date',
                   'Date_Month',
#                   'year',
                   'site_no',
                   'X_72019_00001']

all_wells = stack_all_wells(path = WELL_DATA_PATH_MODIFIED,
                            drop_parameters = drop_parameters)

# after March, before October
#all_wells = all_wells[(all_wells['yday']>60) & (all_wells['yday']<280)]

all_wells['dprcpdt'] = np.nan
all_wells['dtmaxdt'] = np.nan
all_wells['dtmindt'] = np.nan

all_wells['dprcpdt'] = all_wells['prcp (mm/day)'].diff(1)
all_wells['dtmaxdt'] = all_wells['tmax (deg c)'].diff(1)
all_wells['dtmindt'] = all_wells['tmin (deg c)'].diff(1)
all_wells['deltaT'] = all_wells['tmax (deg c)'] - all_wells['tmin (deg c)']

all_wells = all_wells.dropna()
all_wells_subset = all_wells[(all_wells['dhdt']<3) & (all_wells['dhdt']>-3)]

## train test splitting
train_dataset = all_wells_subset.sample(frac=0.8, random_state=0)
test_dataset = all_wells_subset.drop(train_dataset.index)

train_stats = train_dataset.describe()
train_stats.pop("dhdt")
train_stats = train_stats.transpose()

train_labels = train_dataset.pop('dhdt')
test_labels = test_dataset.pop('dhdt')

#normalize
def norm(x):
  return (x - train_stats['mean']) / train_stats['std']
normed_train_data = norm(train_dataset)
normed_test_data = norm(test_dataset)

## tensorflow setups
X = tf.placeholder("float", shape=[None] * normed_train_data.shape[1], name="X")
y = tf.placeholder("float", shape=[None], name="y")


