import numpy as np
import pandas as pd
import os
import calendar
import sys
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import tensorflow as tf
import xgboost as xgb
from util import stack_all_wells
from sklearn.model_selection import train_test_split
from scipy.stats import linregress, zscore

plt.rc('font', family='serif')
limit = 5
z_score_limit = 4

WELL_DATA_PATH_MODIFIED = os.getcwd() + '../data/well-time-series-HPA-modified/'

# resampling opts {'W', 'M', None}
all_wells = stack_all_wells(path = WELL_DATA_PATH_MODIFIED,
                            resampling = None,
                            dropna = False)

unwanted_params = ['US_State',
                   'price_corn',
                   'price_electricity',
                   'number_of_wells',
                   'avg_pump_rate']

all_wells = all_wells.drop(unwanted_params, axis=1)

# after March, before October
all_wells = all_wells[(all_wells['yday']>90) & (all_wells['yday']<270)]

## only wells deeper than 150 ft
#all_wells = all_wells[all_wells['WellDepth_ft']>100]

## interpolate corn prices
#all_wells['price_corn'] = all_wells['price_corn'].interpolate(method='from_derivatives',
#                                                              limit_direction='both', limit=5)

all_wells['deltaT'] = all_wells['tmax (deg c)'] - all_wells['tmin (deg c)']

# add shifts from 1 day to 1 week as new
# features for all climate variables
shifts = range(1,29)
for shift in shifts:
    prcp = 'dprcpdt' + str(shift)
    tmax = 'dtmaxdt' + str(shift)
    tmin = 'dtmindt' + str(shift)
    delT = 'ddeltaTdt' + str(shift)
    dswe = 'swe (kg/m^2)' + str(shift)
    dvp  = 'vp (Pa)' + str(shift)    

    all_wells[prcp] = all_wells['prcp (mm/day)'].diff(shift)
    all_wells[tmax] = all_wells['tmax (deg c)'].diff(shift)
    all_wells[tmin] = all_wells['tmin (deg c)'].diff(shift)
    all_wells[delT] = all_wells['deltaT'].diff(shift)
    all_wells[dswe] = all_wells['swe (kg/m^2)'].diff(shift)
    all_wells[dvp]  = all_wells['vp (Pa)'].diff(shift)

# drop NaNs and make a subset
all_wells_subset = all_wells.dropna()

#all_wells_subset = all_wells[(all_wells['dDTW_dt']<limit) &\
#                             (all_wells['dDTW_dt']>-limit)]

#all_wells_subset = all_wells[(all_wells['dhdt']>0.05) |\
#                             (all_wells['dhdt']<-0.05)]

## outlier removal by z-score
#all_wells_subset = all_wells_subset[(np.abs(zscore(all_wells_subset)) < z_score_limit).all(axis=1)]

all_wells_subset = all_wells_subset.reset_index()

## train test splitting
drops = ['dDTW_dt',
         'DTW_surface_ft',
         'water_level_NGVD29_ft',
         'dwaterlevel_dt']

#X, y = all_wells_subset.drop(drops, axis=1), all_wells_subset['dDTW_dt']
X, y = all_wells_subset.drop(drops, axis=1), all_wells_subset['DTW_surface_ft']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.15, random_state=42)

"""
## XBOOST
def init_xgboost(n_estimator=5000, max_depth=15, lr=0.001, nthread=4):
    xg_reg = xgb.XGBRegressor(objective='reg:squarederror', colsample_bytree=0.99,
                              learning_rate=lr, max_depth=max_depth, #verbose_eval = 4,
                              alpha=0, n_estimators=n_estimator, verbosity=3,
                              nthread=nthread, eta=0.3, max_leaf_nodes=5)

    return xg_reg

def train_xgboost(X_train, y_train, reg):
#    reg.fit(X_train, y_train)
    reg.train(X_train, y_train)

    return reg

def save_prediction(reg, s=5):
#    reg.fit(X_train, y_train)
    mse = mean_squared_error(y_test, reg.predict(X_test))
    rmse = np.sqrt(mse)
    print("RMSE: %.4f" % rmse)

    lin = linregress(y_test, reg.predict(X_test))
    score = reg.score(X_test, y_test)

    lim = [-limit,limit]
    scatter_args = dict(color='k', s=s,
                        label='r$^2$={:0.2f}\nRMSE={:0.2f}'.format(lin.rvalue, rmse),
                        edgecolors='w', linewidth=0.1, alpha=0.75)
    plt.scatter(y_test, reg.predict(X_test), **scatter_args)
    plt.gca().set_aspect('equal')
    plt.grid(linestyle='dotted')
    plt.xlim(lim)
    plt.ylim(lim)
    plt.xlabel('True dDTW_dt (ft/day)')
    plt.ylabel('Predicted dDTW_dt (ft/day)')
    plt.plot(lim, lim, 'grey', alpha=.3, linewidth=1)
    plt.legend()
    plt.savefig('./figs/gbrt/xgboost-test-HPA.png', dpi=300)
    plt.clf()

x_reg = init_xgboost(n_estimator=100, max_depth=100, lr=0.05, nthread=4)
x_reg = train_xgboost(X_train, y_train, x_reg)
save_prediction(x_reg)
"""

def init_and_train_xgboost(param, X_train, y_train):
    dtrain = xgb.DMatrix(data=X_train, label=y_train)
    bst = xgb.train(param, dtrain,  num_boost_round=100)

    return bst

def save_prediction(reg, s=5):
    X_test_DM = xgb.DMatrix(X_test)
    mse = mean_squared_error(y_test, reg.predict(X_test_DM))
    rmse = np.sqrt(mse)
    print("RMSE: %.4f" % rmse)

    lin = linregress(y_test, reg.predict(X_test_DM))

    lim = [-limit,limit]
    scatter_args = dict(color='k', s=s,
                        label='r$^2$={:0.2f}\nRMSE={:0.2f}'.format(lin.rvalue, rmse),
                        edgecolors='w', linewidth=0.1, alpha=0.75)
    plt.scatter(y_test, reg.predict(X_test_DM), **scatter_args)
    plt.grid(linestyle='dotted')
    plt.gca().set_aspect('equal')

#    plt.xlim(lim)
#    plt.ylim(lim)
#    plt.plot(lim, lim, 'grey', alpha=.3, linewidth=1)

    plt.xlim([0,350])
    plt.ylim([0,350])
    plt.plot([0,350], [0,350], 'grey', alpha=.3, linewidth=1)

    plt.xlabel('True dDTW_dt (ft/day)')
    plt.ylabel('Predicted dDTW_dt (ft/day)')
    plt.legend()
    plt.savefig('../figs/gbrt/xgboost-test-HPA.png', dpi=300)
    plt.clf()

xg_params = dict(n_estimator=100, max_depth=80, lr=0.8, nthread=4,
                 colsample_bytree=0.99, objective='reg:squarederror', 
                 learning_rate=0.01, alpha=0, verbosity=3, eta=0.3, max_leaf_nodes=5)
x_reg = init_and_train_xgboost(xg_params, X_train, y_train)
save_prediction(x_reg)

