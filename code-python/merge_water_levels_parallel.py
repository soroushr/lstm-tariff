import os, sys
import calendar
import numpy as np
import pandas as pd
from util import (JulianDate_to_YYYYMMDD,
    months_dict)
import multiprocessing as mp
import crops_dict

## define paths
#####################
WELL_DATA_PATH = os.getcwd() + '/../data/well-time-series-HPA/'
WELL_DATA_PATH_MODIFIED = os.getcwd() + '/../data/well-time-series-HPA-modified-no-crop/'

if not os.path.exists(WELL_DATA_PATH_MODIFIED):
    os.mkdir(WELL_DATA_PATH_MODIFIED)

## load data
#####################
# groundwater data and site info
#gw_data = pd.read_csv(os.getcwd() + '/../data/ALL_HPA/cleaned_data_ngwmn.csv')
#site_info = pd.read_csv(os.getcwd() + '/../data/ALL_HPA/sites_ngwmn.csv')

#gw_data = pd.read_csv(os.getcwd() + '/../data/ALL_HPA/cleaned_data_nwis.csv')
#site_info = pd.read_csv(os.getcwd() + '/../data/ALL_HPA/sites_nwis.csv')

gw_data = pd.read_csv(os.getcwd() + '/../data/ALL_HPA/all_sites_all_vars.csv')
gw_data = gw_data.drop(['var',
                        'id',
                        'crop',
                        'count_cells_crop',
                        'number_of_wells',
                        'avg_pump_rate'], axis=1)

site_info = pd.read_csv(os.getcwd() + '/../data/ALL_HPA/sites_all_sites_all_vars.csv')

"""
# energy price data
price_gas = pd.read_csv(os.getcwd() + '/../data/price-energy/Nebraska_Natural_Gas_Industrial_Price.csv', skiprows=4)
price_electricity = pd.read_csv(os.getcwd() + '/../data/price-energy/electricity_price.csv')

# corn price data
price_corn = pd.read_csv(os.getcwd() + '/../data/price-crops/corn-prices-historical-chart-data.csv', skiprows=15)

# crop by well data
crop_by_well = pd.read_csv(os.getcwd() + '/../data/ALL_HPA/crop_by_well.csv')

# wells within buffer
num_wells_in_buff = pd.read_csv(os.getcwd() + '/../data/ALL_HPA/wells_within_buffer_data.csv')
"""

## dictionary to find the corresponding 
## unique ID of every crop to its string
#####################
crops = crops_dict.crops

# list of all csv.s that should be processed
csv_files = []
for csv_well_data in os.listdir(WELL_DATA_PATH):
    if os.path.splitext(csv_well_data)[-1].lower() != '.csv':
        continue

    csv_files.append(csv_well_data)

def merge_parallel(csv_well_data):
    """
    takes the name of a csv_well_data in the WELL_DATA_PATH
    directory and merges water level and price data and whatnot
    only for one file
    """
    csv_name = os.path.basename(csv_well_data)

    well_data = pd.read_csv(WELL_DATA_PATH + csv_well_data, skiprows=7)

    split = csv_name.split('_')
    lat, lon = float(split[2]), float(split[4])

    site_no = site_info[np.isclose(site_info["dec_lat_va"], lat) &
                        np.isclose(site_info["dec_long_va"], lon)]["site_no"]

    print("###########################")
    print("Latitude is {}".format(lat) )
    print("Longitude is {}".format(lon) )

    # check to make sure the site no.s exist
    if len(site_no.values) == 1:
        print("CSV IN PROGRESS IS {}".format(csv_name) )
        site_no = site_no.values[0]

        # dummy columns that will match the gw_data
        well_data['Date']             = None
        well_data['DTW']              = np.nan
        well_data['State']            = np.nan
#        well_data['count_cells_crop'] = None
#        well_data['number_of_wells']  = None
#        well_data['avg_pump_rate']    = None
        well_data['dDTW_dt']          = np.nan

        well_data['site_no'] = site_no
        for idx, row in well_data.iterrows():
            Date = JulianDate_to_YYYYMMDD(int(row['year']), int(row['yday']))
            well_data.at[idx, 'Date'] = Date

        subset = gw_data.loc[gw_data['site_no'] == site_no]
        subset = subset.drop_duplicates(subset="Date", keep="first")

        for idx, row in subset.iterrows():
            Date              = row['Date']
#            print("fucking date is {}".format(Date) )
#            well_level_depth  = row['DTW']
#            state             = row['State']
#            num_cells         = row['count_cells_crop']
#            num_wells         = row['number_of_wells']
#            pump_rate         = row['avg_pump_rate']

#            if int(Date.split('-')[0]) < 2019:
            well_data.loc[well_data['Date']==Date, 'DTW']              = row['DTW']
            well_data.loc[well_data['Date']==Date, 'State']            = row['State']
#            well_data.loc[well_data['Date']==Date, 'number_of_wells']  = row['number_of_wells']
#            well_data.loc[well_data['Date']==Date, 'avg_pump_rate']    = row['avg_pump_rate']

        well_data['dDTW_dt'] = well_data['DTW'].diff(1)

        ## FIXME
#        # a zero column for each crop
#        for crop_id in crops:
#            well_data[crops[crop_id]] = 0

#        for idx, row in subset.iterrows():
#            _yr     = row['year']
#            _SiteNo = row["site_no"]
#            if _yr < 2019:
#                well_data.loc[ (well_data['year']==_yr) & (well_data['site_no']==_SiteNo), crops[ row['crop']] ] = row['count_cells_crop']

        """
        for idx, row in crop_by_well.iterrows():
            _yr = int(row["state_year"].split("_")[1])
            _SiteNo = row["site_no"]
            if _yr < 2019:
                well_data.loc[ (well_data['year']==_yr) & (well_data['site_no']==_SiteNo), crops[str(row['value'])] ] = row['count_cells_crop']

        # match num. wells within buffer with SiteNo
        well_data['US_State'] = np.nan
        well_data['number_of_wells'] = np.nan
        well_data['avg_pump_rate'] = np.nan

        for idx, row in num_wells_in_buff.iterrows():
            _yr = int(row["year"])
            _state  = row["State"]
            _SiteNo = row["site_no"]
            _avg_p_rate = row["avg_pump_rate"]
            _num_wells = row["number_of_wells"]
            if _yr < 2019:
                well_data.loc[ (well_data['year']==_yr) & (well_data['site_no']==_SiteNo), "US_State" ] = _state
                well_data.loc[ (well_data['year']==_yr) & (well_data['site_no']==_SiteNo), "number_of_wells" ] = _num_wells
                well_data.loc[ (well_data['year']==_yr) & (well_data['site_no']==_SiteNo), "avg_pump_rate" ] = _avg_p_rate


        for idx, row in price_corn.iterrows():
            well_data.loc[well_data['date']==row.date, 'price_corn'] = row['price']

        for idx, row in price_electricity.iterrows():
            well_data.loc[well_data['year']==row.Year, 'price_electricity'] = float(row['price'].split('$')[1])

        well_data['Date_Month'] = well_data['date'].map(lambda x: str(x)[:-3])

        well_data['price_gas'] = np.nan
        for idx, row in price_gas.iterrows():
            _d = row.Month.split(' ')
            if int(_d[1])<2019:
                month_num = months_dict.get(_d[0])
                _date = '-'.join([_d[1], month_num])
                _prc = row['Nebraska Natural Gas Industrial Price  Dollars per Thousand Cubic Feet']
                well_data.loc[well_data['Date_Month']==_date, 'price_gas'] = float(_prc)
        """
        well_data.to_csv(WELL_DATA_PATH_MODIFIED + str(site_no) + '_' + csv_name[:-4] + '_modified.csv')
        print("new saved csv: {}".format(csv_name[:-4] + '_modified.csv') )
    else:
        print('-------------------------------------------')
        print("{} does not seem to have a matching site_no".format(csv_name) )
        print('-------------------------------------------')
        pass



## run parallel
# num. processors 
# leave a few nodes out!
pool = mp.Pool(mp.cpu_count()-2)
if __name__ == '__main__':
    p = pool
    p.map(merge_parallel, [x for x in csv_files] )
