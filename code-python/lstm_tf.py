import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os, sys, math
from sklearn.preprocessing import RobustScaler, MinMaxScaler
from sklearn.metrics import mean_squared_error
import pandas as pd
import tensorflow as tf
from util import preprocess_data, create_dataset
from datetime import datetime
tf.random.set_seed(131)
plt.rc('font', family='serif')

###########################
####### DEFINE PATHS
###########################
FIG_DIR = "./figs-correct/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

LOG_DIR = FIG_DIR + "./logs/"
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)

WELL_DATA_PATH_MODIFIED = os.getcwd() + '/../data/well-time-series-OLD-BAK/well-time-series-HPA-modified/'
CSV_NAME = "400155101521302_11740_lat_40.0286666666667_lon_-101.871222222222_2020-01-01_195632_modified.csv"
###########################
####### DATA PREP
###########################
data = pd.read_csv(WELL_DATA_PATH_MODIFIED + CSV_NAME)
#data.index = pd.to_datetime(data["date"])
# this is just to circumvent the "date" redundency
data.rename(columns={"date": "Date"}, inplace=True)

# resample data to have uniform temporal frequency
RESAMPLE_FQ = 'W'
data_resampled = preprocess_data(data, var_column='DTW_surface_ft', date_column='Date', resampling=RESAMPLE_FQ)

# define the features that are intended to include in training
feature_columns = [
                   'yday',
                   'dayl (s)',
                   'prcp (mm/day)',
                   'srad (W/m^2)',
                   'swe (kg/m^2)',
                   'tmax (deg c)',
                   'tmin (deg c)',
                   'vp (Pa)',
                   'DTW_surface_ft',
#                   'dDTW_dt',
#                   'SiteNo'
                  ]

target_columns = [
                  'DTW_surface_ft',
#                  'dDTW_dt',
                  ]


features = data_resampled[feature_columns]
features = features.dropna(axis=0)

##############################################
####### TRAIN/TEST SPLIT AND PREPROCESSING 
##############################################
TRAIN_RATIO = 0.75
train_size = int(len(features) * TRAIN_RATIO)
test_size = len(features) - train_size
train, test = features.iloc[0:train_size], features.iloc[train_size:len(features)]

print(len(train), len(test))

##########
## scaling
f_transformer = RobustScaler()
vel_transformer = RobustScaler()

# FIXME: because train[['vel']] is also part of the features, a copy
# of it is created to be able to scale it back otherwise the 
# f_transformer brings back both variables.
# fix to avoid replicating the train[['vel']] transformer
train_y_copy, test_y_copy = train[target_columns].copy(), test[target_columns].copy()

f_transformer = f_transformer.fit(train[feature_columns].to_numpy())
vel_transformer = vel_transformer.fit(train_y_copy)

train.loc[:, feature_columns] = f_transformer.transform(train[feature_columns].to_numpy())
train.loc[:, target_columns] = vel_transformer.transform(train_y_copy)

test.loc[:, feature_columns] = f_transformer.transform(test[feature_columns].to_numpy())
test.loc[:, target_columns] = vel_transformer.transform(test_y_copy)

###############
# reformulate to a supervised learning problem
if RESAMPLE_FQ=='D':
    time_steps = 360
elif RESAMPLE_FQ=='W':
    time_steps = 36
elif RESAMPLE_FQ=='M':
    time_steps = 10

# reshape to [samples, time_steps, n_features]
X_train, y_train = create_dataset(train, train[target_columns], time_steps)
X_test, y_test = create_dataset(test, test[target_columns], time_steps)

################
# training specs

# define log path for tensorboard
logdir = LOG_DIR + datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)

# define the network structure
model = tf.keras.Sequential()
model.add(
    tf.keras.layers.Conv1D(
        filters=8, kernel_size=4,
        strides=1, padding="causal",
        activation=None#"relu"
  )
)
model.add(
  tf.keras.layers.Bidirectional(
    tf.keras.layers.LSTM(
      units=8, 
      input_shape=(X_train.shape[1], X_train.shape[2]),
      return_sequences = True
    )
  )
)
model.add(tf.keras.layers.Dropout(rate=0.2))
model.add(
  tf.keras.layers.LSTM(
    units=4,
    input_shape=(X_train.shape[1], X_train.shape[2])
  )
)
model.add(tf.keras.layers.Dropout(rate=0.5))
model.add(tf.keras.layers.Dense(units=8))
#model.add(tf.keras.layers.Dropout(rate=0.5))
model.add(tf.keras.layers.Dense(units=1))

#optimizer = tf.keras.optimizers.SGD()
optimizer = tf.keras.optimizers.Adam(learning_rate=0.001,
                                     beta_1=0.9,
                                     beta_2=0.999)
loss = tf.keras.losses.Huber()
#loss = 'mean_squared_error'
model.compile(loss=loss, optimizer=optimizer)

history = model.fit(
    X_train, y_train, 
    epochs=1000, 
    batch_size=100,
    validation_split=0.1,
    shuffle=True,
    callbacks=[tensorboard_callback]
)

print("Average test loss: ", np.average(history.history['loss']))

## check losses
plt.plot(history.history['loss'], label='train', alpha=0.75)
plt.plot(history.history['val_loss'], label='validation', alpha=0.75)
plt.grid(linestyle='dotted')
plt.xlabel("# epochs")
plt.ylabel("loss")
plt.legend(); plt.savefig(FIG_DIR + "well-train-val-loss.png", dpi=300)
plt.clf()

## prediction
y_pred = model.predict(X_test)

# revert and scale back
y_train_inv = vel_transformer.inverse_transform(y_train.reshape(1, -1))
y_test_inv = vel_transformer.inverse_transform(y_test.reshape(1, -1))
y_pred_inv = vel_transformer.inverse_transform(y_pred)

rmse = math.sqrt(mean_squared_error(y_test_inv.reshape(-1, 1), y_pred_inv))
################
## plots results
plt.plot(np.arange(0, len(y_train)), y_train_inv.flatten(), 'g', label="history")
plt.plot(np.arange(len(y_train), len(y_train) + len(y_test)), y_test_inv.flatten(), marker='.', label="true", alpha=0.6)
plt.plot(np.arange(len(y_train), len(y_train) + len(y_test)), y_pred_inv.flatten(), 'r', label="prediction", alpha=0.6)
plt.ylabel('DTW [ft]')
plt.xlabel('Time Step')
plt.legend(loc='best')
plt.title('RMSE: %.3f [ft]' % rmse)
plt.savefig(FIG_DIR + "well-check.png", dpi=300)
plt.clf()

## plot zoomed in to validation set
plt.plot(y_test_inv.flatten(), label='true', alpha=0.7, linewidth=2, marker='o', markersize=2, color='C0')
plt.plot(y_pred_inv.flatten(), label='prediction', alpha=0.7, linewidth=2, marker='s', markersize=2, color='r')
plt.ylabel('DTW [ft]')
plt.xlabel('Time Step')
plt.xticks(rotation=0)
plt.title('RMSE: %.3f [ft]' % rmse)
plt.legend(); plt.grid(linestyle='dotted')
plt.savefig(FIG_DIR + "well-check-zoom.png", dpi=300)
plt.clf()

