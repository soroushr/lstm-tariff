import os, sys
import calendar
import numpy as np
import pandas as pd
from util import (JulianDate_to_YYYYMMDD,
    months_dict)
import crops_dict

## define paths
#####################
#WELL_DATA_PATH = os.getcwd() + '/data/well-time-series-HPA/'
#WELL_DATA_PATH_MODIFIED = os.getcwd() + '/data/well-time-series-HPA-modified/'

WELL_DATA_PATH = os.getcwd() + '../data/temp-well-time-series-HPA/'
WELL_DATA_PATH_MODIFIED = os.getcwd() + '../data/temp-well-time-series-HPA-modified/'
if not os.path.exists(WELL_DATA_PATH_MODIFIED):
    os.mkdir(WELL_DATA_PATH_MODIFIED)

## load data
#####################
# groundwater data
gw_data = pd.read_csv(os.getcwd() + '../data/ALL_HPA/cleaned_data.csv')

# energy price data
price_gas = pd.read_csv(os.getcwd() + '../data/price-energy/Nebraska_Natural_Gas_Industrial_Price.csv', skiprows=4)
price_electricity = pd.read_csv(os.getcwd() + '../data/price-energy/electricity_price.csv')

# corn price data
price_corn = pd.read_csv(os.getcwd() + '../data/price-crops/corn-prices-historical-chart-data.csv', skiprows=15)

# original site infor data
site_info = pd.read_csv(os.getcwd() + '../data/ALL_HPA/sites.csv')
lats, lons = site_info.DecLatVa, site_info.DecLongVa

# crop by well data
crop_by_well = pd.read_csv(os.getcwd() + '../data/ALL_HPA/crop_by_well.csv')

# wells within buffer
num_wells_in_buff = pd.read_csv(os.getcwd() + '../data/ALL_HPA/wells_within_buffer_data.csv')

## dictionary to find the corresponding 
## unique ID of every crop to its string
#####################
crops = crops_dict.crops

## run
#####################
count = 0
for csv_well_data in os.listdir(WELL_DATA_PATH):
    if os.path.splitext(csv_well_data)[-1].lower() != '.csv':
        continue

    csv_name = os.path.basename(csv_well_data)

    well_data = pd.read_csv(WELL_DATA_PATH + csv_well_data, skiprows=7)

    split = csv_name.split('_')
    lat, lon = float(split[2]), float(split[4])

    site_no = site_info[np.isclose(site_info['DecLatVa'], lat) &
                        np.isclose(site_info['DecLongVa'], lon)].SiteNo
    if len(site_no.values) == 1:
        print("CSV IN PROGRESS IS {}".format(csv_name) )
        site_no = site_no.values[0]

        # dummy columns that will match the gw_data
        well_data['date'] = None
#        well_data['X_72019_00001'] = np.nan
        well_data['DTW_surface_ft'] = np.nan
        well_data['water_level_NGVD29_ft'] = np.nan
        well_data['WellDepth_ft'] = np.nan
        well_data['AltVa_ft'] = np.nan
        well_data['min_DTW'] = np.nan

        well_data['dDTW_dt'] = np.nan
        well_data['dwaterlevel_dt'] = np.nan
        well_data['price_gas'] = np.nan
    
        well_data['SiteNo'] = site_no
        for idx, row in well_data.iterrows():
            Date = JulianDate_to_YYYYMMDD(int(row['year']), int(row['yday']))
            well_data.at[idx, 'date'] = Date

        subset = gw_data.loc[gw_data['SiteNo']==site_no]

        for idx, row in subset.iterrows():
            Date              = row['date']
            well_level_depth  = row['DTW_surface_ft']
            water_level       = row['water_level_NGVD29_ft']
            well_depth        = row['WellDepth_ft']
            surface_elevation = row['AltVa_ft']
            min_DTW           = row['min_DTW']

            if int(Date.split('-')[0]) < 2019:
                well_data.loc[well_data['date']==Date, 'DTW_surface_ft'] = well_level_depth
                well_data.loc[well_data['date']==Date, 'water_level_NGVD29_ft'] = water_level
                well_data.loc[well_data['date']==Date, 'WellDepth_ft'] = well_depth
                well_data.loc[well_data['date']==Date, 'AltVa_ft'] = surface_elevation
                well_data.loc[well_data['date']==Date, 'min_DTW'] = min_DTW

        well_data['dDTW_dt'] = well_data['DTW_surface_ft'].diff(1)
        well_data['dwaterlevel_dt'] = well_data['water_level_NGVD29_ft'].diff(1)

        # a zero column for each crop
        for crop_id in crops:
            well_data[crops[crop_id]] = 0

        for idx, row in crop_by_well.iterrows():
            _yr = int(row["state_year"].split("_")[1])
            _SiteNo = row["SiteNo"]
            if _yr < 2019:
                well_data.loc[ (well_data['year']==_yr) & (well_data['SiteNo']==_SiteNo), crops[str(row['value'])] ] = row['count']

        # match num. wells within buffer with SiteNo
        well_data['US_State'] = np.nan
        well_data['number_of_wells'] = np.nan
        well_data['avg_pump_rate'] = np.nan

        for idx, row in num_wells_in_buff.iterrows():
            _yr = int(row["year"])
            _state  = row["State"]
            _SiteNo = row["SiteNo"]
            _avg_p_rate = row["avg_pump_rate"]
            _num_wells = row["number_of_wells"]
            if _yr < 2019:
                well_data.loc[ (well_data['year']==_yr) & (well_data['SiteNo']==_SiteNo), "US_State" ] = _state
                well_data.loc[ (well_data['year']==_yr) & (well_data['SiteNo']==_SiteNo), "number_of_wells" ] = _num_wells
                well_data.loc[ (well_data['year']==_yr) & (well_data['SiteNo']==_SiteNo), "avg_pump_rate" ] = _avg_p_rate

        for idx, row in price_corn.iterrows():
            well_data.loc[well_data['date']==row.date, 'price_corn'] = row['price']

        for idx, row in price_electricity.iterrows():
            well_data.loc[well_data['year']==row.Year, 'price_electricity'] = float(row['price'].split('$')[1])

        well_data['Date_Month'] = well_data['date'].map(lambda x: str(x)[:-3])
        for idx, row in price_gas.iterrows():
            _d = row.Month.split(' ')
            if int(_d[1])<2019:
                month_num = months_dict.get(_d[0])
                _date = '-'.join([_d[1], month_num])
                _prc = row['Nebraska Natural Gas Industrial Price  Dollars per Thousand Cubic Feet']
                well_data.loc[well_data['Date_Month']==_date, 'price_gas'] = float(_prc)

#        well_data.drop(['Date_Month'], axis=1)
        well_data.to_csv(WELL_DATA_PATH_MODIFIED + str(site_no) + '_' + csv_name[:-4] + '_modified.csv')
        count += 1
        print("#{} saved csv: {}".format(count, csv_name[:-4] + '_modified.csv') )

    else:
        print('-------------------------------------------')
        print("{} does not seem to have a matching site_no".format(csv_name) )
        print('-------------------------------------------')
        pass


