import numpy as np
import pandas as pd
import os
import calendar
import sys
import matplotlib.pyplot as plt
from util import stack_all_wells
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression, SGDClassifier
from sklearn.preprocessing import MinMaxScaler
from scipy.stats import linregress, zscore

plt.rc('font', family='serif')

WELL_DATA_PATH_MODIFIED = os.getcwd() + '../data/well-time-series-modified/'
limit = 5
z_score_limit = 4

all_wells = stack_all_wells(path = WELL_DATA_PATH_MODIFIED,
                            resampling = None,
                            dropna = False)

# after March, before October
all_wells = all_wells[(all_wells['yday']>90) & (all_wells['yday']<260)]

# interpolate corn prices
all_wells['price_corn'] = all_wells['price_corn'].interpolate(method='from_derivatives',
                                                              limit_direction='both', limit=5)

all_wells['deltaT'] = all_wells['tmax (deg c)'] - all_wells['tmin (deg c)']

# add shifts from 1 day to 1 week as new
# features for all climate variables
shifts = range(1,8)
for shift in shifts:
    prcp = 'dprcpdt' + str(shift)
    tmax = 'dtmaxdt' + str(shift)
    tmin = 'dtmindt' + str(shift)
    delT = 'ddeltaTdt' + str(shift)
    dswe = 'swe (kg/m^2)' + str(shift)
    dvp  = 'vp (Pa)' + str(shift)    

    all_wells[prcp] = all_wells['prcp (mm/day)'].diff(shift)
    all_wells[tmax] = all_wells['tmax (deg c)'].diff(shift)
    all_wells[tmin] = all_wells['tmin (deg c)'].diff(shift)
    all_wells[delT] = all_wells['deltaT'].diff(shift)
    all_wells[dswe] = all_wells['swe (kg/m^2)'].diff(shift)
    all_wells[dvp]  = all_wells['vp (Pa)'].diff(shift)

all_wells = all_wells.dropna()
all_wells_subset = all_wells[(all_wells['dhdt']<limit) & (all_wells['dhdt']>-limit)]

# outlier removal by z-score
#all_wells_subset = all_wells_subset[(np.abs(zscore(all_wells_subset)) < z_score_limit).all(axis=1)]

all_wells_subset = all_wells_subset.reset_index()

## train test splitting
drops = ['dhdt']#, 'srad (W/m^2)']
X, y = all_wells_subset.drop(drops, axis=1), all_wells_subset['dhdt']
#X, y = all_wells_subset[['price_corn', 'price_electricity', 'price_gas']], all_wells_subset['dhdt']

#data_dmatrix = xgb.DMatrix(data=X,label=y)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=1)

## XBOOST
def init_gbrt(n_estimator=5000, max_depth=15, lr=0.001, min_samples_split=2, nthread=4):

    params = {'n_estimators': n_estimator, 'max_depth': max_depth,
              'min_samples_split': min_samples_split,
              'learning_rate': lr, 'loss': 'ls'}
    gbrt_reg = GradientBoostingRegressor(**params)

    return gbrt_reg

def train_gbrt(X_train, y_train, reg):
    reg.fit(X_train, y_train)

    return reg    

def save_prediction(reg, s=.1):
#    reg.fit(X_train, y_train)
    mse = mean_squared_error(y_test, reg.predict(X_test))
    rmse = np.sqrt(mse)
    print("RMSE: %.4f" % rmse)

    lin = linregress(y_test, reg.predict(X_test))
    score = reg.score(X_test, y_test)

    lim = [-limit,limit]
    scatter_args = dict(color='k', s=s,
                        label='r$^2$={:0.2f}\nRMSE={:0.2f}'.format(lin.rvalue, rmse) )
    plt.scatter(y_test, reg.predict(X_test), **scatter_args)
    plt.gca().set_aspect('equal')
    plt.grid(linestyle='dotted')
    plt.xlim(lim)
    plt.ylim(lim)
    plt.xlabel('True dhdt (m/day)')
    plt.ylabel('Predicted dhdt (m/day)')
    plt.plot(lim, lim, 'grey', alpha=.3, linewidth=1)
    plt.legend()
    plt.savefig('../figs/gbrt/gbrt-test.png', dpi=300)
    plt.clf()

x_reg = init_gbrt(n_estimator=1000, lr=0.05, nthread=10)
x_reg = train_gbrt(X_train, y_train, x_reg)
save_prediction(x_reg)

"""
drop_parameters = [#'Date',
                   'Date_Month',
#                   'year',
                   'site_no']

all_wells = stack_all_wells(path = WELL_DATA_PATH_MODIFIED,
                            resampling = 'W')

# after March, before October
all_wells = all_wells[(all_wells['yday']>90) & (all_wells['yday']<270)]

#all_wells['dprcpdt'] = np.nan
#all_wells['dtmaxdt'] = np.nan
#all_wells['dtmindt'] = np.nan

all_wells['dprcpdt'] = all_wells['prcp (mm/day)'].diff(1)
all_wells['dtmaxdt'] = all_wells['tmax (deg c)'].diff(1)
all_wells['dtmindt'] = all_wells['tmin (deg c)'].diff(1)
#all_wells['deltaT'] = all_wells['tmax (deg c)'] - all_wells['tmin (deg c)']

all_wells = all_wells.dropna()
all_wells_subset = all_wells[(all_wells['dhdt']<3) & (all_wells['dhdt']>-3)]

## train test splitting
X, y = all_wells_subset.drop(['dhdt'], axis=1), all_wells_subset['dhdt']

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.20, random_state=42)

## RANDOM FOREST
def save_randomForest():
    regr = RandomForestRegressor(max_depth=8, random_state=123,
                                 n_estimators=1000)

    regr.fit(X_train, y_train)
    mse = mean_squared_error(y_test, regr.predict(X_test))
    print("MSE: %.4f" % mse)

    plt.scatter(y_test, regr.predict(X_test), color='r', s=2)
    plt.gca().set_aspect('equal')
    plt.grid()
    lim = [-3,3]
    plt.xlim(lim)
    plt.ylim(lim)
    plt.xlabel('True dhdt (m/day)')
    plt.ylabel('Predicted dhdt (m/day)')
    plt.savefig('./figs/gbrt/RF-test.png',dpi=300)
    plt.clf()

## GRADIENT BOOSTING REGRESSION TREE
def save_gbrt(n_estimator=1000, max_depth=10, lr=0.05):
    params = {'n_estimators': n_estimator, 'max_depth': max_depth, 'min_samples_split': 2,
              'learning_rate': lr, 'loss': 'ls'}
    gbrt_regressor = GradientBoostingRegressor(**params)

    gbrt_regressor.fit(X_train, y_train)
    mse = mean_squared_error(y_test, gbrt_regressor.predict(X_test))
    print("MSE: %.4f" % mse)

    lin = linregress(y_test, gbrt_regressor.predict(X_test))
    scatter_args = dict(color='r', s=1,
                        label='r$^2$={:0.2f}'.format(lin.rvalue) )
    plt.scatter(y_test, gbrt_regressor.predict(X_test), **scatter_args)
    plt.gca().set_aspect('equal')
    plt.grid()
    lim = [-3,3]
    plt.xlim(lim)
    plt.ylim(lim)
    plt.legend()
    plt.grid(linestyle='dotted')
    plt.xlabel('True dhdt (m/day)')
    plt.ylabel('Predicted dhdt (m/day)')
    plt.savefig('./figs/gbrt-test-%i.png'%n_estimator,dpi=300)
    plt.clf()

## runs
save_gbrt(n_estimator=1000, lr=0.05)
"""
