import numpy as np
import pandas as pd
import os, sys
import calendar
import matplotlib.pyplot as plt
import tensorflow as tf

months_dict = {'Jan':'01',
               'Feb':'02',
               'Mar':'03',
               'Apr':'04',
               'May':'05',
               'Jun':'06',
               'Jul':'07',
               'Aug':'08',
               'Sep':'09',
               'Oct':'10',
               'Nov':'11',
               'Dec':'12'}

# a function to match match the
# date format to that of the gw_data
def JulianDate_to_YYYYMMDD(year, jd):
    """Takes the year and Julian day as
    integers and creates a string of YYYY-MM-DD
    Args:
        - year: integer
        - jd: Julian day, integer
    Returns:
        - 'YYYY-MM-DD', string
    """ 
    month = 1
    day = 0
    while jd - calendar.monthrange(year, month)[1] > 0 and month <= 12:
        jd = jd - calendar.monthrange(year, month)[1]
        month = month + 1

    year, month, jd = str(year), str(month).zfill(2), str(jd).zfill(2)
    return '-'.join([year, month, jd])


def stack_all_wells(path, dropna=True, resampling=False):
    """Takes all the .csv well files in a folder
    and stack them on top of each other, and drops
    the specified parameters
    Args:
        - path: specifies the path to the csv files, string
        - drop_parameters: list of parameters that are to be dropped
        - resampling scheme: takes 'D', 'W', or 'M'
    Returns:
        - all_wells: pd.DataFrame containing all the well data
    """
    unwanted_params = [
                       'Date_Month',
                       'year',
#                       'X_72019_00001',
                       'SiteNo']

    all_wells = pd.DataFrame()
    for csv_well_data in os.listdir(path):
        if os.path.splitext(csv_well_data)[-1].lower() != '.csv':
            continue

        csv_name = os.path.basename(csv_well_data)

        well_data = pd.read_csv(path + csv_well_data, parse_dates=['date'])
        well_data = well_data.drop(unwanted_params, axis=1)

        if dropna:
            well_data = well_data.dropna()

        if resampling:
            resampled = well_data.reset_index().set_index('date',drop=True).resample(resampling).mean()
            all_vars = resampled#.drop(drop_parameters, axis=1)
        else:
            resampled = well_data.reset_index().set_index('date',drop=False)
            all_vars = well_data.drop(['date'], axis=1)
            
        all_wells = all_wells.append(all_vars, ignore_index=True)
        print("appended {}".format(csv_name) )
    
    return all_wells.loc[:, ~all_wells.columns.str.contains('^Unnamed')]


def preprocess_data(data, var_column='y', date_column='ds', resampling='M'):
    # TODO: add capabilities for different resampling methods
    """Preprocesses time-series data to create uniform temporal   
    frequency and fills in the gaps with a give interpolation scheme

    Args:
        data: DataFrame of time series [pd.DataFrame]
        var_column: name of the column containing variable [string]
        date_column: name of the column containing time [string]
        resampling: resampling frequency [string]
                    options: {'D', 'W', 'W', 'M'}

    Returns:
        data_resamled: DataFrame with uniform sampling frequency [pd.DataFrame]
    """
    data.index = pd.to_datetime(data[date_column])
    data.index.name = 'date'

    data_resampled = data.reset_index().set_index('date', drop=True).resample(resampling).mean()

    data_resampled[var_column] = data_resampled[var_column].interpolate(method='index')

    return data_resampled


def create_dataset(X, y, time_steps=1):
    """Takes the training and test sequences and
    returns training and test vectors

    Args:
        X: matrix containing all the features [numpy array]
        y: matrix containing all the targets [numpy array]

    Returns:
        np.array(Xs): windowed data of features
        np.array(ys): windowed data of targets
    """
    Xs, ys = [], []
    count = 0
    for i in range(len(X) - time_steps):
        v = X.iloc[i:(i + time_steps)].values
        Xs.append(v)
        ys.append(y.iloc[i + time_steps])

        if np.any(np.isnan(v)):
            raise ValueError("there are nan values in v array")

    return np.array(Xs), np.array(ys)

