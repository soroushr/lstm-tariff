site_info = fread("/Users/Mani/Dropbox/Research Collaborations/Soroush-Mani Research Folder/Data/ALL_HPA/SITE_INFO.csv")
# site_info = site_info[AquiferType == "UNCONFINED",.(AgencyCd, SiteNo, DecLongVa, DecLatVa, 
#                                                     StateCd, CountyCd, WlWellPurpose, AltVa_ft = AltVa, WellDepth_ft = WellDepth)]
site_info[, SiteNo := as.character(SiteNo)]
# coord = site_info[,.(DecLongVa, DecLatVa)]
# foo_2 = SpatialPointsDataFrame(coord, data = site_info[,.(SiteNo)])
# plot(foo_2)

# casing = fread("/Users/Mani/Dropbox/Research Collaborations/Soroush-Mani Research Folder/Data/ALL_HPA/SCREEN.csv")
# casing = casing[,.(AgencyCd, SiteNo, ScreenDepthFrom_ft = ScreenDepthFrom, ScreenDepthTo_ft = ScreenDepthTo)]
# casing[, SiteNo := as.character(SiteNo)]
# 
# setkey(casing, AgencyCd, SiteNo)
# setkey(site_info, AgencyCd,   SiteNo)
# site_info=casing[site_info]
# site_info=unique(site_info, by=c("AgencyCd","SiteNo"))

# ggplot(site_info[WellDepth_ft<1000], aes(x=ScreenDepthFrom_ft, y=WellDepth_ft))+
#   geom_point()

water_level = fread("/Users/Mani/Dropbox/Research Collaborations/Soroush-Mani Research Folder/Data/ALL_HPA/WATERLEVEL.csv")
water_level[, date  := substr(Time, 1, 10)]
water_level[, date  := as.Date(date, "%Y-%m-%d")]
water_level[, year  := year(date)]
water_level[, month := month(date)]
water_level[, day := format(date, format = "%d")]
water_level[, Time := NULL]
water_level = unique(water_level, by=c("AgencyCd", "SiteNo", "date"))
water_level[, count :=.N, by=c("AgencyCd", "SiteNo")]
water_level = water_level[count>100]
water_level = water_level[,.(AgencyCd, SiteNo, parameter =`Original Parameter`, date, year, month, day, count, DTW_surface_ft= `Depth to Water Below Land Surface in ft.`, 
                             water_level_NGVD29_ft = `Water level in feet relative to NGVD29`)]
# water_level[,count := .N, by=c("AgencyCd", "SiteNo", "date")]
water_level = unique(water_level, by=c("AgencyCd", "SiteNo", "date"))
setkey(water_level, AgencyCd, SiteNo)
setkey(site_info,   AgencyCd, SiteNo)


# # water_level[, unique(SiteNo)]
# # site_info[, unique(SiteNo)]
# 
water_level[, SiteNo:=as.character(SiteNo)]
site_info[, SiteNo:=as.character(SiteNo)]
setkey(water_level, SiteNo)
setkey(site_info, SiteNo)

foo= unique(water_level, by="SiteNo")
foo=site_info[foo]
foo[is.na(DecLatVa)]

water_level = water_level[site_info]
water_level = water_level[complete.cases(AgencyCd) & complete.cases(DecLongVa) & complete.cases(date)]
setkey(water_level, AgencyCd, SiteNo, date)
water_level = unique(water_level, by=c("AgencyCd", "SiteNo", "date"))
water_level[, count :=.N, by=c("AgencyCd", "SiteNo")]
# water_level = water_level[WellDepth_ft>100]
water_level[,.N, by=c("AgencyCd", "SiteNo")]


water_level[, DTW_surface_ft := as.numeric(DTW_surface_ft)]
water_level[, min_DTW := min(DTW_surface_ft), by=c("AgencyCd", "SiteNo")]
water_level[, min_DTW := as.numeric(min_DTW)]
water_level[, max_DTW := max(DTW_surface_ft), by=c("AgencyCd", "SiteNo")]
water_level[, max_DTW := as.numeric(max_DTW)]
water_level[, Link := NULL]
water_level[, i.AgencyCd := NULL]

ggplot(water_level[SiteNo==405014099591001], aes(x=date, y=-DTW_surface_ft))+
  geom_point()+
  theme_bw() +
  scale_x_date(labels = date_format("%m-%Y"))


# water_level[!(max_DTW>15 | min_DTW>10)]
# water_level = water_level[min_DTW>10]
# water_level[min_DTW>20]

# ggplot(water_level[SiteNo==415559098005201], aes(x=date, y=water_level_NGVD29_ft))+
#   geom_point()

water_level[, SiteNo := paste0(" ", SiteNo)]

write.csv(water_level, "/Users/Mani/Dropbox/Research Collaborations/lstm-tariff/data/ALL_HPA/cleaned_data_ngwmn.csv")

sites = water_level[,.(SiteNo, DecLongVa, DecLatVa)]
sites = unique(sites, by="SiteNo")

write.csv(sites, "/Users/Mani/Dropbox/Research Collaborations/lstm-tariff/data/ALL_HPA/sites_ngwmn.csv")

